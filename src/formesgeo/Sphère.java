package formesgeo;
import static java.lang.Math.*;

public class Sphère extends Piece{
    
    private double rayon;
    
     public Sphère( String repere, String couleur, double densite, double rayon) {
  
       super(repere,couleur, densite);
       
       this.rayon=rayon;
  }
    @Override
     public double volume(){return 4/3 * PI * pow(rayon,3);}
    @Override
     public void afficher(){
        System.out.print("\nSPHERE ");
   
   super.afficher();
   
   System.out.printf("Rayon:  %6.2f cm\n", rayon);
  
   System.out.printf("Volume: %6.2f cm3 Poids:  %6.2f g\n" , volume(), poids());
    }

    public double getRayon() {
        return rayon;
    }

    public void setRayon(double rayon) {
        this.rayon = rayon;
    }
     
}
